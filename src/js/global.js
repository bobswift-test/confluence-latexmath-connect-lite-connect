window.LatexMath = window.LatexMath || {};

$(document).on('latexmath-ap-loaded', 'body', function() {
    var $body, $mathelement, $caption, $mathparent, $bodyInput, $anchorInput,
        LATEXMATH_ERROR = 'latexmath-error',
        UPDATE_CAPTION_EVENT = 'updateCaption';

    var frameList = getAllLatexMathFrames();

    $body = $('body');
    $caption = $("#caption");
    $mathparent = $(".math");
    $mathelement = $("#math");
    $bodyInput = $('#math-body');
    $anchorInput = $('#math-anchor');

    if (LatexMath.hasMath() && !LatexMath.isEditor()) {
        if (LatexMath.isInline()) {
            $mathelement.html('\\( \\)');
        } else {
            $mathelement.html('\\[ \\]');
        }
    }

    if (!LatexMath.isEditor() && LatexMath.macro && LatexMath.macro !== LatexMath.MATHINLINE) {
        if (LatexMath.getUrlParam('lic') !== 'active') {
            handleError('No license found for LaTeX Math App');
            return;
        }
    }

    function htmlEntities(str) {
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    }

    if ($caption.length > 0) {
        $body.on(UPDATE_CAPTION_EVENT, function (e, data) {
            $caption.text(makeCaption(data.number));
            LatexMath.captionNum = data.number;

            if (LatexMath.macro === LatexMath.MATHBLOCKREF) {
                AP.getLocation(function (location) {
                    $caption.attr('href', location.replace(/#.*/, '') + '#ap-' + data.frameId)
                });
                afterReferenceRender();
            }
        });
    }

    function getAllLatexMathFrames(parent, list) {
        parent = parent || top;
        list = list || [];
        for (var i = 0; i < parent.frames.length; i++) {
            if (iframeHasSameOrigin(parent.frames[i])) {
                list.push(parent.frames[i]);
            }
            getAllLatexMathFrames(parent.frames[i], list);
        }
        return list;
    }

    function iframeHasSameOrigin(global) {
        var key = ( +new Date ) + "" + Math.random();

        try {
            global[key] = "asd";
            var result = global[key] === "asd";
            delete global[key];
            return result;
        }
        catch (e) {
            return false;
        }
    }

    function allIframesInitialized() {
        for (var i = 0; i < frameList.length; i++) {
            if (!frameList[i].LatexMath) {
                return false;
            }
        }
        return true;
    }

    function resizeFrame($content, width, height) {
        $content.width(getValue(width));
        if (height) {
            $content.height(getValue(height));
        }
        // This is crazy, but seems to be necessary:
        setTimeout(function() {AP.resize(getValue(width))}, 1);
        setTimeout(function() {AP.resize(getValue(width))}, 100);
        setTimeout(function() {AP.resize(getValue(width))}, 500);
        setTimeout(function() {AP.resize(getValue(width))}, 750);
        setTimeout(function() {AP.resize(getValue(width))}, 1000);
        setTimeout(function() {AP.resize(getValue(width))}, 5000);

        function getValue(val) {
            if ($.isFunction(val)) {
                return val();
            } else {
                return val;
            }
        }
    }

    function handleError(errMsg) {
        $mathparent.removeClass('loading');
        $mathparent.removeClass('math');
        $mathparent.spinStop();
        $mathparent.addClass('error');
        $mathelement.hide();
        $('.ac-content').first().prepend(''
            + '<div class="error-container">'
            + '<div class="aui-message error shadowed" id="' + LATEXMATH_ERROR + '">'
            + '<p class="title"><span class="aui-icon icon-error"></span>'
            + '<strong>An error occured</strong>'
            + '</p>'
            + errMsg
            + '</div>'
            + '</div>');

        $('#' + LATEXMATH_ERROR).width(400);
        var $errorDiv = $('div.error-container');
        resizeFrame($errorDiv.parent(), $errorDiv.outerWidth(), $errorDiv.outerHeight());
    }

    function removeError() {
        $mathparent.addClass('math');
        $mathparent.removeClass('error');
        $('.error-container').remove();
    }

    function makeCaption(number) {
        return '(' + number + ')';
    }

    function renderMath(mathbody, callback) {
        removeError();
        mathbody = LatexMath.decodeLatex(mathbody);
        if (!window.MathJax || !MathJax.Hub || !MathJax.Hub.Queue) {
            console.log('MathJax not ready yet, waiting to render math');
            setTimeout(renderMath, 10, mathbody, callback);
        } else {
            MathJax.Hub.Queue(function () {
                try {
                    var mathnode = MathJax.Hub.getAllJax("math")[0];
                    MathJax.Hub.Queue(["Text",mathnode,mathbody]);
                    if (callback) { MathJax.Hub.Queue(callback); }
                } catch (err) {
                    console.trace(err);
                    handleError(err.message);
                }
            });
        }
    }

    function afterMathRender() {
        $mathparent.removeClass('loading');
        $mathparent.spinStop();
        $mathelement.show();
        if (LatexMath.macro === LatexMath.MATHINLINE) {
            var $MathJaxSpan = $('#math .MathJax .math');
            resizeFrame($mathelement.parent(), $MathJaxSpan.width(), $MathJaxSpan.height()+1);
        } else {
            AP.resize();
        }
    }

    function afterReferenceRender() {
        console.log('Done rendering');
        $mathparent.removeClass('loading');
        $mathparent.spinStop();
        $caption.show();
        resizeFrame($caption.parent(), $caption.width(), $caption.height());
    }

    if (LatexMath.hasMath() && !LatexMath.isEditor()) {
        $mathparent.spin();
        $mathelement.hide();

        AP.confluence.getMacroBody(function(body) {
            renderMath(body || LatexMath.getUrlParam('body'), afterMathRender);
        });
    }

    if (!LatexMath.isEditor() && LatexMath.macro === LatexMath.MATHBLOCK && LatexMath.anchor) {
        // Get caption number for block macro
        var blockCnt = 0;
        for (var i = 0; i < frameList.length; i++) {
            var frame = frameList[i];
            if (frame.LatexMath && frame.LatexMath.anchor && frame.LatexMath.macro === LatexMath.MATHBLOCK) {
                blockCnt++;
                frame.$('body').trigger(UPDATE_CAPTION_EVENT, {number: blockCnt, frameId: frame.LatexMath.frameId});
            }
        }
    }

    if (!LatexMath.isEditor() && LatexMath.macro === LatexMath.MATHBLOCKREF) {
        var checkCountAfterInit = 4;

        // Get caption number for block reference
        function checkIfLoaded() {
            if (!LatexMath.captionNum) {
                for (var i = 0; i < frameList.length; i++) {
                    var frame = frameList[i];
                    if (frame.LatexMath
                        && frame.LatexMath.anchor === LatexMath.anchor
                        && frame.LatexMath.macro === LatexMath.MATHBLOCK) {
                        if (frame.LatexMath.captionNum) {
                            $body.trigger(UPDATE_CAPTION_EVENT, {
                                number: frame.LatexMath.captionNum,
                                frameId: frame.LatexMath.frameId
                            });
                            afterReferenceRender();
                        } else {
                            frame.$('body').on(UPDATE_CAPTION_EVENT, function (e, data) {
                                $body.trigger(UPDATE_CAPTION_EVENT, data);
                                afterReferenceRender();
                            });
                        }
                        return;
                    }
                }

                if (allIframesInitialized()) {
                    checkCountAfterInit--;
                }

                if (checkCountAfterInit>0) {
                    setTimeout(checkIfLoaded, 250);
                } else {
                    handleError('Could not find Math Block with anchor=' + htmlEntities(LatexMath.anchor));
                }
            }
        }

        checkIfLoaded();
    }

    if (LatexMath.hasMath() && LatexMath.isEditor()) {
      AP.require(["confluence", "dialog"], function (confluence, dialog) {
        confluence.getMacroBody(function (body) {
          $bodyInput.val(LatexMath.decodeLatex(body));
        });

        confluence.getMacroData(function (params) {
          params = params || {};

          if (LatexMath.macro === LatexMath.MATHBLOCK) {
            $anchorInput.parent().show();
          } else {
            $anchorInput.parent().hide();
          }

          if (LatexMath.isInline()) {
            $bodyInput.val(LatexMath.decodeLatex(params["body"] || ''));
          }

          $anchorInput.val(params["anchor"]);
        });

        function onSubmit() {
          var getParams = function () {
            var params = {};

            var setParam = function (key, element) {
              var val = element.val();
              if (val !== "") params[key] = val;
            };

            setParam("anchor", $anchorInput);

            if (LatexMath.isInline()) {
              setParam("body", $bodyInput);
              if (params["body"]) params["body"] = LatexMath.encodeLatex(params["body"]);
            }

            return params;
          };

          var getBody = function () {
            if (!LatexMath.isInline()) {
              return $bodyInput.val();
            }
            return "";
          };

          confluence.saveMacro(getParams(), getBody());
          confluence.closeMacroEditor();
          return true;
        }

        dialog.getButton("submit").bind(onSubmit);

        $(window).keydown(function (event) {
          if ((event.keyCode === 10 || event.keyCode === 13) && event.ctrlKey) {
            onSubmit();
          }
        });

        $("#math-body").focus()
      });
    }
});
