(function() {
    window.LatexMath = window.LatexMath || {};

    LatexMath.getUrlParam = function (param) {
        param = param.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + param + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };

    LatexMath.anchor = LatexMath.getUrlParam('anchor');
    LatexMath.frameId = (LatexMath.getUrlParam('xdm_c') || '').replace(/^channel-/, '');
    LatexMath.macro = LatexMath.getUrlParam('macro') || String(location).replace(/.*macros\/(math[a-z\-]+)\/?\?.*/g, '$1');
    LatexMath.MATHINLINE = 'mathinline';
    LatexMath.MATHBLOCK = 'mathblock';
    LatexMath.MATHBLOCKREF = 'mathblock-ref';
    LatexMath.hasMath = function () {
        return [LatexMath.MATHINLINE, LatexMath.MATHBLOCK].indexOf(LatexMath.macro) >= 0;
    };

    LatexMath.isInline = function() {
        return LatexMath.macro === LatexMath.MATHINLINE;
    };

    LatexMath.isEditor = function() {
        return document.documentElement.classList.contains("editor");
    };

    if (LatexMath.macro.indexOf('math') !== 0) {
        console.log('Warning: could not find macro type');
        LatexMath.macro = '';
    }

    var encodingKey = '--uriencoded--';
    LatexMath.encodeLatex = function(latex) {
      var encoded = latex.replace(/%/g, '%25').replace(/\^/g, '%5e').replace(/{/g, '%7B').replace(/}/g, '%7D').replace(/\|/g, '%7C');
      return encoded !== latex ? encodingKey+encoded : latex;
    };

    LatexMath.decodeLatex = function(latex) {
      if (latex && latex.slice(0, encodingKey.length) === encodingKey) {
        return decodeURIComponent(latex.slice(encodingKey.length));
      } else {
        return latex;
      }
    };

    // Load MathJax
    if (LatexMath.hasMath() && !LatexMath.isEditor()) {
        // MathJax settings
        window.MathJax = {
            extensions: ["tex2jax.js", "MathZoom.js", "MathMenu.js", "toMathML.js", "TeX/noErrors.js", "TeX/noUndefined.js", "TeX/AMSmath.js", "TeX/AMSsymbols.js", "TeX/color.js"],
            jax: ["input/TeX", "output/HTML-CSS", "output/NativeMML"],
            FontWarnings: {imageFont: null},
            showMathMenu: false,
            showMathMenuMSIE: false,
            messageStyle: "none",
            showProcessingMessages: false
        };

        var script = document.createElement("script");
        script.src = 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-AMS_HTML-full,Safe';
        script.referrerPolicy = 'no-referrer';
        document.head.appendChild(script);
    }
}());
