const common = require('./webpack.common')
const {merge} = require('webpack-merge')
const TerserPlugin = require('terser-webpack-plugin'); // To minify the JS

module.exports = merge( common, {
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin({
      test: /\.js(\?.*)?$/i,
    })]
  }
});