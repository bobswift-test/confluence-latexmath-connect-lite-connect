// Allow the AWS config to be loaded based on the profile value 
const common = require('./webpack.common')
const {merge} = require('webpack-merge')
const path = require('path');
process.env.AWS_SDK_LOAD_CONFIG = true;
var AWS = require("aws-sdk");
var credentials = new AWS.SharedIniFileCredentials({profile: process.env.AWS_PROFILE});
AWS.config.credentials = credentials;
AWS.config.update({region: 'us-east-1'});
var dynamoDb = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10' });
const tableName = 'appfire-atlassian-persist-prod-AppSettings';

module.exports = merge( common, {
  devtool: "inline-source-map",
  mode: "development",
  devServer: {
      contentBase: path.join(__dirname, "./client/dist/"),
      publicPath: path.join(__dirname, "./client/dist/"),
      port: 9000,
      public: '*.ngrok.io',
      disableHostCheck: true,
      before: function(app, server, compiler) {
        app.post('/lifecycle', async function(req, res) {
          const timestamp = new Date().getTime();
          let resData = '';
          req.on('data', function (chunk) {
            resData += chunk;
          });
          req.on('end', async () => {
            data=JSON.parse(resData);
            console.log('data.key:'+data.key)
            if (data.eventType.toLowerCase() == "installed") {
              if (data.sharedSecret == null || data.clientKey == null) {
                var errorMsg = 'Unable to process the request because of failure in getting required fields.';
                console.error(errorMsg);
                throw new Error(errorMsg);
              }
              params = {
                TableName: tableName,
                Item: {
                    "key": data.key,
                    "clientKey": data.clientKey,
                    "sharedSecret": data.sharedSecret,
                    "oauthClientId": data.oauthClientId,
                    "pluginsVersion": data.pluginsVersion,
                    "baseUrl": data.baseUrl,
                    "productType": data.productType,
                    "description": data.description,
                    "sen": data.serviceEntitlementNumber,
                    "status": data.eventType,
                    "createdAt": timestamp,
                    "updatedAt": timestamp
                  }
                };
              try {
                await create(params);
                res.send('Successfully Installed!');
              } catch(e) {
                console.error(e);
                res.sendStatus(500);
              }
            } else {
              res.send('Successfully Installed!');
            }
          })
        });
        function create(params) {
          const promise = new Promise(function (resolve, reject) {
              dynamoDb.put(params, function (err, data) {
                  if (err) {
                      reject(err);
                  } else {
                      resolve(data);
                  }
              });
          });
          return promise;
        }
      }
    },
});
