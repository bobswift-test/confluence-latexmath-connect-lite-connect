const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin=require('mini-css-extract-plugin');
const glob = require('glob');
const ReplaceInFileWebpackPlugin = require('replace-in-file-webpack-plugin');
const PurifyCSSPlugin=require('purifycss-webpack');

const envKeys = Object.keys(process.env).reduce((prev, next) => {
  prev[`process.env.${next}`] = JSON.stringify(process.env[next]);
  return prev;
}, {});

module.exports = {
  entry: {
    init: './src/js/init.js',
    global: './src/js/global.js'
  },
  output: {
    path: path.resolve(__dirname, './client/dist'),
    //publicPath: '/client/dist',
    filename: './js/[name].js'
  },
  plugins: [
    new webpack.DefinePlugin(envKeys),
    new ReplaceInFileWebpackPlugin([{
      dir: 'client/dist',
      files: ['atlassian-connect.json'],
      rules: [{
          search: /\%domain\%/g,
          replace: process.env.AWS_APP_DOMAIN
      },{
        search: /\%api_stage\%/g,
        replace: process.env.API_STAGE == "" ? "" : `-${process.env.API_STAGE}`
      },{
        search: /\%api_path\%/g,
        replace: process.env.API_PATH
      }]
    }]),
    new MiniCssExtractPlugin({
      filename:"[name].css"
    }),
    new CopyPlugin([
      { from: './atlassian-connect.json', to: './' },
      { from: './src/img/*.*', to: './img/', flatten: true },
      { from: './src/resources/*.*', to: './src/resources/', flatten: true },
      { from: './src/index.html',  to: './' },
      { from: './src/components/macros/matheditor/*.html',  to: './macros/matheditor/', flatten: true },
      { from: './src/components/macros/mathinline/*.html',  to: './macros/mathinline/', flatten: true },
      { from: './src/components/macros/mathblock/*.html',  to: './macros/mathblock/', flatten: true },
      { from: './src/components/macros/mathblock-ref/*.html',  to: './macros/mathblock-ref/', flatten: true },
      { from: './src/css/*.css',  to: './css/', flatten: true },
    ]),
    new PurifyCSSPlugin({
      // Give paths to parse for rules. These should be absolute!
      paths: glob.sync(path.join(__dirname, 'src/**/*.html')),
      purifyOptions: {
        info: true,
        whitelist: [ '*:not*' ]
      }
    })],
  resolve: {
    extensions: ['.js'],
  },
  module: {
    rules: [
      {
        //ecma 2015 transpiling for browser compatibility
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react']
          }
        }
      },
      {
        //bundles all css/stylesheets
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          //'style-loader',
          'css-loader'
        ]
      },
      {
        //bundles all image files
        test: /\.(png|jpg|gif)$/,
        use: [
          {loader: 'url-loader'}
        ]
      }
    ]
  },
  externals: {
    jQuery: 'jQuery'
  }
}


