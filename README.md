# LaTeX Math Lite by Bolo Atlassian Apps (an Appfire company)
Include basic support for LaTeX within Confluence. This app renders math client-side using MathJax. Please see LaTeX Math for more features such as faster (server-side) render, support for exporting to PDF/Word, more macros, and more options for customizing your experience. The full version is available for both Cloud and Server and besides a full feature set has received excellent reviews. If you decide to upgrade to the full version later, your LaTeX Math Lite macros will continue to work. (But please disable LaTeX Math Lite before installing/enabling LaTeX Math to avoid macro conflicts)


## AppKey: confluence-latexmath-connect-lite-connect

# Prerequisites
## Local Proxy 
Typical for App Developer Setup.

[Setup of AWS on Local Machine](https://wiki.appfire.com/display/CBW/Initial+Setup+of+AWS+on+Local+Machine)

[Setup of Appfire Connect SDK](https://wiki.appfire.com/pages/viewpage.action?pageId=156600301)

[Setup of Local App Proxy](https://wiki.appfire.com/display/CBW/Local+App+Development+Proxy+Setup)

## Additional Prerequisites for standalone AWS environment 
Typical for AC Core Developers only. This is not required to deploy or run your apps locally for testing purposes. Only follow these additional steps if you intent to run a separate, personal instance of Appfire Connect.

[Initial Setup of Your AWS Instance](https://wiki.appfire.com/display/CBW/Initial+Setup+of+Developer+AWS+Instance)

# Deployment Instructions

## Setup
Configure your personal.env.yml for deployment to sandbox AWS environments
```
environment:
personal:
    domain: latexmath-connect-lite-dev.bolo.<add-personal-domain-name-here>
    profile: default
    stage: personal
    api-stage: prod | stage | snapshot #<--default 'prod', for app descriptor webhook tokenization
```
Once a personal.env.yml is in place. Run your App locally against the Appfire Connect DTS Production-mirro test environment. 

## Deploy & Run Locally
```
appfire run 
```
or run against other Appfire Connect DTS environments with the "--api-stage" switch
```
appfire run --api-stage
```

## Standalone Deployment of Appfire Connect Core (optional)

### 1) Deploy the Core Stack to an AWS Instance

First time setup on new AWS instance:
```
appfire bootstrap
```
Then deploy (re-deploy) of Appfire Connect core services is a simple command.
```
appfire deploy core -s
```

### 2) Deploy the App to Appfire Connect stack
```
appfire deploy
```

## Destroy the App Distribution
This destroys the App distribution in CloudFront. Not the AC Core CloudFormation stack if running standalone.
```
appfire destroy
```

## Full Appfire Connect SDK Help
This will display the help that is listed below at any time.
```
appfire --help
```

```
Usage: appfire [OPTIONS] COMMAND
  Gathers information related to deployment and deploys the CDK stack
  Commands:
    bootstrap     Bootstrap the CDK toolkit
    build         Build the app
      Options:  --profile, --env, --stage, --stack, --api-domain, --api-stage
    build:dev     Build the app with webpack-dev.config.js config
      Options:  --profile, --env, --stage, --stack, --api-domain, --api-stage
    deploy        Build and deploy the specified stack
      Options:  --profile, --stack
    diff          Diff the local stack with deployed stack
      Options:  --profile
    synth         Generates AWS CloudFormation template from the app
      Options:  --profile
    destroy       Destroy the specified stack
      Options:  --profile, --stack
    list          List the stacks for the app
      Options:  --profile
    run           Build and run the app with webpack-dev.config.js config
      Options:  --profile, --env, --stage, --stack, --api-domain, --api-stage
    version       Print version information for the Appfire SDK
  Learn more - https://wiki.appfire.com/x/x6gjCQ
  Documentation - https://appfire.bitbucket.io/
Options:
  -v, --verbose                   Verbose output
  -p, --profile TEXT              AWS profile as the default environment
  -e, --env TEXT                  standalone, dts or prod
  -s, --stack TEXT                CDK stack to deploy
  -stage, --stage TEXT            dev, test, stage or prod
  -as, --app-suffix TEXT          blue or green
  -b, --build TEXT                True or False
  -api-domain, --api-domain TEXT  API domain for Core
                                  Services(common.appfire.app or
                                  dts.common.appfire.app or services.custom-
                                  domain)
  -api-stage, --api-stage TEXT    API stage for Core Services(prod or snapshot
                                  or stage)
  -require-approval, --require-approval TEXT
                                  never or any-change or broadening
  -h, --help                      Show this message and exit.
```

# Addtional Instructions

For full details and help see the [Appfire Connect How-To Guides](https://wiki.appfire.com/display/CBW/Appfire+Connect+How-Tos)

# Links 
* [Internal Development Wiki](https://bolosoftware.atlassian.net/wiki/spaces/DOC/pages/720732161/LaTeX+Math+Lite)
* [Jira Project](https://bolosoftware.atlassian.net/browse/LML)
* [Marketplace Link](https://marketplace.atlassian.com/apps/1214386/latex-math-lite?tab=overview)
* [Customer Support Portal](https://bolosoftware.atlassian.net/servicedesk/customer/portal/2)
* [Customer Documentation](https://bolosoftware.atlassian.net/wiki/spaces/DOC/overview?homepageId=196652)

Copyright © 2021 Appfire Technologies, LLC.